# Firmware test suite
fwts tests suite for exercising and testing different aspects of a machine's firmware. \
Test Maintainer: [Jiri Dluhos](mailto:jdluhos@redhat.com)

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
